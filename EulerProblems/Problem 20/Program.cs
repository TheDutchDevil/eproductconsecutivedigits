﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Problem_20
{
    class Program
    {
        /*
         n! means n  (n  1)  ...  3  2  1

For example, 10! = 10  9  ...  3  2  1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
         */
        static void Main(string[] args)
        {
            BigInteger answerLong = ExclamationMarkNumber(100);
            Console.WriteLine(ExclamationMarkNumber(100));

            string answer = answerLong.ToString();

            int sum = 0;
            foreach (char c in answer)
            {
                sum += int.Parse(c.ToString());
            }

            Console.WriteLine(sum);
            Console.ReadLine();
        }

        private static BigInteger ExclamationMarkNumber(int number)
        {
            BigInteger answer = number;

            for (int i = number - 1; i > 0; i--)
            {
                answer = answer * i;
            }

            return answer;
        }
    }
}

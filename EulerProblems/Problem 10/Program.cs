﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Problem_10
{
    class Program
    {
        private static bool _done = false;

        private static volatile List<int> primesLowerThan2Mill = new List<int>();
        /// <summary>
        /// Summation of primes
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            
            ThreadPool.SetMaxThreads(16, 16);
            for (int i = 2; i <= 2000000; i++)
            {
                    ThreadPool.QueueUserWorkItem(ThreadPoolCallback, i);
            }

            while (!_done) Thread.Sleep(5000);
            Console.WriteLine("Done with crunching the numbers, {0} prime numbers were found. Total of their sum is: ", Program.primesLowerThan2Mill.Count);
            
            
            Console.ReadLine();

            long total = primesLowerThan2Mill.Take(primesLowerThan2Mill.Count).Sum();
            Console.Write(total);
            Console.ReadLine();
            Console.ReadLine();
        }

        private static void ThreadPoolCallback(Object threadContext)
        {
            int i = (int) threadContext;
            if (IsNumberPrime(i))
            {
                primesLowerThan2Mill.Add(i);
                Console.WriteLine("New prime number: {0}", i);
            }

            if (i == 2000000)
            {
                _done = true;
            }
        }

        private static bool IsNumberPrime(int number)
        {
            for (int i = 2; i < (number / 2) + 1; i++)
            {
                double result = (double)number / (double)i;

                if ((result) % 1 == 0)
                {
                    return false;
                }

            }

            return true;
        }

    }
}
